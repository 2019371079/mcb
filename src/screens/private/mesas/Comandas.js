import React from 'react';
import { View, Text, Dimensions } from 'react-native';
// import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { createMaterialTopTabNavigator } from '@react-navigation/material-top-tabs';
import { FontAwesome5, MaterialIcons, Entypo } from '@expo/vector-icons';
import NuevaOrden from './comandas/NuevaOrden';
import Mesas from './comandas/Mesas';
import getProductos from '../productos/getProductos';

const Tab = createMaterialTopTabNavigator();

const Comandas = () => {
    return (
        <Tab.Navigator
            initialRouteName='NuevaOrden'
            tabBarPosition='top'
            initialLayout={{ width: Dimensions.get('window').width }}
            screenOptions={{
                tabBarLabelStyle: { fontSize: 12 },
                tabBarStyle: { backgroundColor: '#252525' },
                tabBarActiveTintColor: 'aqua',
            }}
        >
            <Tab.Screen
                name='nueva orden'
                component={NuevaOrden}
                options={{
                    tabBarIcon: () => (
                        <MaterialIcons name='post-add' size={28} color='#fff' />
                    ),
                }}
            />

            <Tab.Screen
                name='Mesas'
                component={Mesas}
                options={{
                    tabBarIcon: () => (
                        <MaterialIcons
                            name='restaurant'
                            size={28}
                            color='#fff'
                        />
                    ),
                }}
            />

            <Tab.Screen
                name='Productos'
                component={getProductos}
                options={{
                    tabBarIcon: () => (
                        <Entypo name='user' size={28} color='#fff' />
                    ),
                }}
            />
        </Tab.Navigator>
    );
};

export default Comandas;
